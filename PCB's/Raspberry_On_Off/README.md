# Raspberry On Off 

PCB to turn programatically shut down and reboot the Raspberry Pi with a switch. 

For this to work a script mus be used, see ours [here](https://gitlab.com/tecnico.solar.boat/2018/pcbs/raspberry-on-off/snippets/1793562).

More info can be found in the following [link](https://othermod.com/raspberry-pi-soft-onoff-circuit/).




Copyright (C) 2018  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

