/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/


// -------------------------------------------------------------
// tsb_print.cpp
// -------------------------------------------------------------

#include <lcd_print.h>

void TSB_PRINT::init_logos(LiquidCrystal tsb_lcd)
{
	uint8_t line=0;
	for(uint8_t i=0; i<LOGO_ROWS; i++){
		for(uint8_t k=0; k<LCD_ROWS; k++){
			for(uint8_t j=0; j<LOGO_COLUMNS; j++){
				tsb_lcd.setCursor(j, k);
				if((i+k) >= LOGO_ROWS) line = i+k-LOGO_ROWS;
				else line = i+k;
				if(BOOT_LOGOSline[line][j]) tsb_lcd.write(0);
				else tsb_lcd.print(" ");
			}
		}
		digitalWrite(BUZZERPIN, true);
		digitalWrite(LEDPIN_RED1, true); delay(15);
		digitalWrite(BUZZERPIN, false);
		digitalWrite(LEDPIN_RED1, false); digitalWrite(LEDPIN_GRN1, true); delay(15);
		digitalWrite(LEDPIN_GRN1, false); digitalWrite(LEDPIN_RED2, true); delay(15);
		digitalWrite(LEDPIN_RED2, false); digitalWrite(LEDPIN_GRN2, true); delay(15);
		digitalWrite(LEDPIN_GRN2, false); digitalWrite(LEDPIN_RED3, true); delay(15);
		digitalWrite(LEDPIN_RED3, false); digitalWrite(LEDPIN_GRN3, true); delay(15);
		digitalWrite(LEDPIN_GRN3, false); digitalWrite(LEDPIN_RED3, true); delay(15);
		digitalWrite(LEDPIN_RED3, false); digitalWrite(LEDPIN_GRN2, true); delay(15);
		digitalWrite(LEDPIN_GRN2, false); digitalWrite(LEDPIN_RED2, true); delay(15);
		digitalWrite(LEDPIN_RED2, false); digitalWrite(LEDPIN_GRN1, true); delay(15);
		digitalWrite(LEDPIN_GRN1, false); digitalWrite(LEDPIN_RED1, true); delay(15);
		digitalWrite(LEDPIN_RED1, false); delay(15);

	}

	tsb_lcd.clear();
	tsb_lcd.setCursor(6,1);
	tsb_lcd.print(startMsgString);
}

void TSB_PRINT::reset_var_lcd(LiquidCrystal tsb_lcd)
{
	//tsb_lcd.clear();

	tsb_lcd.setCursor(0,0);
	tsb_lcd.print("RPM:");

	tsb_lcd.setCursor(0,3);
	tsb_lcd.print("PWR_INP:");

	tsb_lcd.setCursor(20,3);
	tsb_lcd.print("T_MOS:");

	tsb_lcd.setCursor(11,0);
	tsb_lcd.print("SPD:      ");

	tsb_lcd.setCursor(21,0);
	tsb_lcd.print("VBAT:      ");

	tsb_lcd.setCursor(0,1);
	tsb_lcd.print("PBAT:      ");

	tsb_lcd.setCursor(11,1);
	tsb_lcd.print("PSOL:      ");

	tsb_lcd.setCursor(21,1);
	tsb_lcd.print("BATSTAT:           ");
}

void TSB_PRINT::update_time(LiquidCrystal tsb_lcd)
{
	String seconds, minutes, hours, timeString;

	if(minute()<10) minutes = "0"+String(minute());
	else minutes = String(minute());

	if(second()<10) seconds = "0"+String(second());
	else seconds = String(second());

	if(hour()<10) hours = "0"+String(hour());
	else hours = String(hour());

	timeString = hours + ":" + minutes + ":" + seconds;

	tsb_lcd.setCursor(32,0);
	tsb_lcd.print(timeString);
}

void TSB_PRINT::update_mctrl(LiquidCrystal tsb_lcd, float power,float rpm, float temp_mos)
{
	tsb_lcd.setCursor(4,0);
	tsb_lcd.print(String(rpm,2));
	tsb_lcd.setCursor(6,3);
	tsb_lcd.print(String(power,2));
	tsb_lcd.setCursor(26 , 3);
	tsb_lcd.print(String(temp_mos,2));
	//delay(75);

	/*if(warning != 0)
	{
		tsb_lcd.setCursor(0,2);
		tsb_lcd.print(mctrl_war_msg[warning-1]);
	}*/
}

void TSB_PRINT::update_fctrl(LiquidCrystal tsb_lcd, float speed)
{
	tsb_lcd.setCursor(15,0);
	tsb_lcd.print(String(speed,2));
	//delay(75);
}

void TSB_PRINT::update_bctrl(LiquidCrystal tsb_lcd, float batpower, float solarpower, float voltage, uint8_t status, uint8_t warning)
{
	tsb_lcd.setCursor(26,0);
	tsb_lcd.print(String(voltage,2));

	tsb_lcd.setCursor(5,1);
	tsb_lcd.print(String(batpower));

	tsb_lcd.setCursor(16,1);
	tsb_lcd.print(String(solarpower));
	//delay(75);
	/*if(status != 0)
	{
		tsb_lcd.setCursor(30,1);
		tsb_lcd.print(bat_status_msg[status-1]);
	}

	if(warning != 0)
	{
		tsb_lcd.setCursor(15,3);
		int warnings[4];
		for(int i =0; i<4; i++) warnings[i]= warning & byte((2*exp(i)));
		for(int i =0; i<4; i++){
				if(warnings[i] != 0){
					Serial.println(bat_war_msg[i]);
					tsb_lcd.print(bat_war_msg[i]);
				}
		}
	}*/
}

void TSB_PRINT::heartbeat_blink(void)
{
	bool hb_ledState = true;
	for(uint8_t i=0; i<4; i++){
		digitalWrite(LEDPIN_GRN1, hb_ledState);
		digitalWrite(LEDPIN_GRN2, hb_ledState);
		digitalWrite(LEDPIN_GRN3, hb_ledState);
		hb_ledState = !hb_ledState;
		//delay(75);
	}
}

void TSB_PRINT::warning_blink(void)
{
	bool war_ledState = true;
	digitalWrite(BUZZERPIN, true);
	for(uint8_t i=0; i<2; i++){
		digitalWrite(LEDPIN_RED1, war_ledState);
		digitalWrite(LEDPIN_RED2, war_ledState);
		digitalWrite(LEDPIN_RED3, war_ledState);
		war_ledState = !war_ledState;
		delay(25);
	}
	digitalWrite(BUZZERPIN, false);
}
void TSB_PRINT::print_msg(LiquidCrystal tsb_lcd,tsb_serial_msg_t msg)
{
	tsb_lcd.setCursor(4,0);
	tsb_lcd.print(String(msg.addr,2));
	tsb_lcd.setCursor(15,0);
	tsb_lcd.print(String(msg.data_id,2));
	tsb_lcd.setCursor(26,0);
	tsb_lcd.print(String(msg.data_size,2));
}
