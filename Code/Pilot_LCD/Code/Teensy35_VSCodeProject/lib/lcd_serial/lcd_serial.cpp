/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
*/


// -------------------------------------------------------------
// tsb_serial.cpp
// -------------------------------------------------------------

#include <lcd_serial.h>

/***********************************************************************
 * Name:    send_msg
 * Args:    tsb_serial_msg
 * Return:  None
 * Desc:    Synchronous serial transmit message. Shall be called after
 			a serial message respecting the TSB communication protocol
 			is built to be sent to the database relay microcontroller.
 ***********************************************************************/
void TSB_SERIAL::send_msg(tsb_serial_msg_t msg)
{
	uint8_t checksum = 0, tx_buf[msg.data_size + MSG_NODATA_SIZE];
	tx_buf[0] = MSG_START_BYTE;
	tx_buf[1] = msg.addr; checksum ^= tx_buf[1];
	tx_buf[2] = msg.data_id; checksum ^= tx_buf[2];
	for(uint8_t i = MSG_DATA_OFFSET; i < MSG_DATA_OFFSET + msg.data_size; i++){
		tx_buf[i] = msg.data[i-MSG_DATA_OFFSET]; checksum ^= tx_buf[i];
	}
	tx_buf[MSG_DATA_OFFSET + msg.data_size] = checksum;
	tx_buf[MSG_DATA_OFFSET + msg.data_size + 1] = MSG_END_BYTE;

	Serial.write(tx_buf, MSG_NODATA_SIZE + msg.data_size);
}

/***********************************************************************
 * Name:    receive_msg
 * Args:    None
 * Return:  tsb_serial_msg
 * Desc:    Polling serial receive message. Shall be periodically called
 			to check for received messages through USB respecting the TSB
 			communication protocol.
 ***********************************************************************/
tsb_serial_msg_t TSB_SERIAL::receive_msg()
{
	uint8_t incomingByte=0, checksum=0, msg_size=0;
	bool receiving = false;
	uint8_t uart_rxbuf[MSG_MAX_SIZE];
	tsb_serial_msg_t tsb_msg;

	tsb_msg.valid_msg = false;

 	// Poll USB buffer for received data
 	if (Serial.available() > 0) {
		while (Serial.available() > 0) {
			incomingByte = Serial.read();
			// MSG_START_BYTE signals packet incoming
			if(incomingByte == MSG_START_BYTE && !receiving){
				uart_rxbuf[msg_size++] = incomingByte;
				receiving = true;
			}
			// MSG_END_BYTE signals packet ending
			else if(incomingByte == MSG_END_BYTE){
				uart_rxbuf[msg_size++] = incomingByte;
				receiving = false;
			}
			// Otherwise keep receiving data
			else if(receiving){
				uart_rxbuf[msg_size++] = incomingByte;
			}
		}
	}
	else{
		return tsb_msg;
	}

	// Compute checksum with relevant bytes
	for(uint8_t i=1; i < (msg_size-2); i++) checksum ^= uart_rxbuf[i];

	// Return valid message after checking checksum
	if (checksum == uart_rxbuf[msg_size-2]) {
		tsb_msg.valid_msg = true;
		tsb_msg.addr = uart_rxbuf[1];
		tsb_msg.data_id = uart_rxbuf[2];
		tsb_msg.data_size = msg_size - MSG_NODATA_SIZE;
		for(uint8_t i = 0; i < tsb_msg.data_size; i++){
			tsb_msg.data[i] = uart_rxbuf[MSG_DATA_OFFSET + i];
		}
	}
	return tsb_msg;
}

void TSB_SERIAL::decode_motormsg(tsb_serial_msg_t msg)
{
	// Firstly decode raw values
	Power_Input.raw = (uint32_t) (msg.data[0] << 24) + (msg.data[1] << 16) + (msg.data[2] << 8) + msg.data[3];
	RPM.raw = (uint32_t) (msg.data[4] << 24) + (msg.data[5] << 16) + (msg.data[6] << 8) + msg.data[7];
	TempMos.raw = (uint32_t) (msg.data[8] << 24) + (msg.data[9] << 16) + (msg.data[10] << 8) + msg.data[11];

	// Calculate physical values
	Power_Input.phy = *(float*)& Power_Input.raw;
	RPM.phy = *(float*)& RPM.raw;
	RPM.phy = RPM.phy / (-5);
	TempMos.phy =*(float*)& TempMos.raw;
	TempMos.phy = TempMos.phy / 10.0;	
}

void TSB_SERIAL::decode_foilsmsg(tsb_serial_msg_t msg)
{
	// Firstly decode raw values
	speedX.raw = (uint32_t) (msg.data[3] << 24) + (msg.data[2] << 16) + (msg.data[1] << 8) + msg.data[0];
	speedY.raw = (uint32_t) (msg.data[7] << 24) + (msg.data[6] << 16) + (msg.data[5] << 8) + msg.data[4];

	// Calculate physical values
	speedX.phy = *(float*)& speedX.raw;
	speedY.phy = *(float*)& speedY.raw;

	speed = sqrt(speedX.phy*speedX.phy + speedY.phy*speedY.phy);
}

void TSB_SERIAL::decode_bmsmsg(tsb_serial_msg_t msg)
{
	// Firstly decode raw values
	VBat.raw = (int32_t) (msg.data[0] << 24) + (msg.data[1] << 16) + (msg.data[2] << 8) + msg.data[3];
	BatPower.raw = (int32_t) (msg.data[4] << 24)+(msg.data[3] << 16)+(msg.data[2] << 8) + msg.data[1];
	SolarPower.raw = 0;
	BatStatus = 0;
	BatWarnings = 0;

	// Calculate physical values
	VBat.phy = *(float*)& VBat.raw;
	BatPower.phy = *(float*)& BatPower.raw;
	SolarPower.phy = *(float*)& SolarPower.raw;
}
