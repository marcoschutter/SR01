/*
 * TSB main.cpp
 */

/***********************************************************************
 * This main file serves the purpose of testing the tsb_serial API
 * that implements the communication between a Teensy and a Raspberry Pi
 ***********************************************************************/

#include <tsb_serial.h>

#define LEDPIN 			LED_BUILTIN

TSB_SERIAL tsb_serial_class;
bool LEDstate=true;
tsb_serial_msg_t tsb_msg;

// -------------------------------------------------------------
void setup(void)
{
	delay(3000);
	pinMode(LEDPIN, OUTPUT);

/* 	tsb_msg.addr = 0x04;
	tsb_msg.data_id = 0x03;
	tsb_msg.data_size = MSG_MAXDATA_SIZE;
	for(uint8_t i = 0; i < MSG_MAXDATA_SIZE ; i++) tsb_msg.data[i] = i*i; */
}

// -------------------------------------------------------------
void loop(void)
{
	digitalWrite(LEDPIN, LEDstate);
	LEDstate = !LEDstate;
	
	// tsb_serial_class.send_msg(tsb_msg);

	tsb_msg = tsb_serial_class.receive_msg();

	Serial.print("\n");
	if(tsb_msg.valid_msg == true){
		Serial.print(tsb_msg.addr, DEC); Serial.print(" ");
		Serial.print(tsb_msg.data_id, DEC); Serial.print(" ");
		for(uint8_t i = 0; i < tsb_msg.data_size; i++){
			Serial.print(tsb_msg.data[i], DEC); Serial.print(" ");
		}
		Serial.print("\n");
	}
	else{
		Serial.print("NOT FOUND");
	}

	delay(1000);
}