/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', { 'packages': ['gauge'] });
google.charts.setOnLoadCallback(I_M0);
google.charts.setOnLoadCallback(P_M0);
google.charts.setOnLoadCallback(RPM);
google.charts.setOnLoadCallback(T_Mot0);
google.charts.setOnLoadCallback(VOLTAGE_Mot0);

var mysqlDataMotores;

setInterval(function() {
    $.ajax({
        url: "php/Motores.php",
        dataType: "JSON",
        data: {},
        success: function(x) {
            mysqlDataMotores = x;
        }
    });   
}, 1000);

function I_M0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I M0', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 175,
        redFrom: 160, redTo: 175,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 5,
        majorTicks: ['0', '25', '50', '75', '100', '125', '150', '175' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('I_M0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["CurrentInput"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function P_M0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['P M0', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 175,
        redFrom: 160, redTo: 175,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 5,
        majorTicks: ['0', '1', '2', '3', '4', '5', '6' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'W x1000',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('P_M0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["PowerInput"]/1000);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function I_MT() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I MT', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 350,
        //redFrom: 160, redTo: 175,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 5,
        majorTicks: ['0', '50', '100', '150', '200', '250', '300' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('I_MT'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, (mysqlDataMotores["Current_M0"] + mysqlDataMotores["Current_M1"]));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}

function VOLTAGE_Mot0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['M0 V', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 35,
        max: 51,
        redFrom: 50.4, redTo: 51,
        yellowFrom: 35, yellowTo: 36, yellowColor: '#DC3912',
        
        minorTicks: 4,
        majorTicks: ['35', '39', '43', '47', '51']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 2,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('VOLTAGE_Mot0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["VoltInput"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function VOLTAGE_Mot1(){
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['M1 V', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 35,
        max: 51,
        redFrom: 50.4, redTo: 51,
        yellowFrom: 35, yellowTo: 36, yellowColor: '#DC3912',
        
        minorTicks: 4,
        majorTicks: ['35', '39', '43', '47', '51']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 2,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('VOLTAGE_Mot1'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["BatteryVoltage_M1"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}


function RPM() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['RPM', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 6,
        redFrom: 5,
        redTo: 6,
        yellowFrom: 4,
        yellowTo: 5,
        minorTicks: 5,
        majorTicks: ['0', '1', '2', '3', '4', '5', '6']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'x1000',
		fontsize: 7,
        fractionDigits: 1,
        decimalSymbol: ',',
        groupingSymbol: '.',
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('RPM'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, Math.abs(mysqlDataMotores["RPM"]/1000));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function T_Mot0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['T Mos', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 150,
        redFrom: 70,
        redTo: 150,
        yellowFrom: 50,
        yellowTo: 70,
        minorTicks: 5,
        majorTicks: ['0', '50', '100', '150']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
		fontsize: 7,
        fractionDigits: 0,
        decimalSymbol: ',',
        groupingSymbol: '.',
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('T_Mot0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseInt(mysqlDataMotores["MosTemp1"]));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function T_Mot1() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['T M1', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 0,
        max: 150,
        redFrom: 70,
        redTo: 150,
        yellowFrom: 50,
        yellowTo: 70,
        minorTicks: 5,
        majorTicks: ['0', '50', '100', '150']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
		fontsize: 7,
        fractionDigits: 0,
        decimalSymbol: ',',
        groupingSymbol: '.',
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('T_Mot1'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseInt(mysqlDataMotores["Temperature_M1"]));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}

function Torque_M0() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['\u01AE M0', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 14,
        redFrom: 10, redTo: 14,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 2,
        majorTicks: ['0', '2', '4', '6', '8', '10', '12', '14' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'Nm',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('Torque_M0'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["Current_M0"] * 0.079788);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}

function Torque_M1() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['\u01AE M1', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 14,
        redFrom: 10, redTo: 14,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 2,
        majorTicks: ['0', '2', '4', '6', '8', '10', '12', '14' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'Nm',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('Torque_M1'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["Current_M1"] * 0.079788);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}

function Asked_Torque() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['\u01AE ?', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 28,
        redFrom: 20, redTo: 28,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 50,
        minorTicks: 2,
        majorTicks: ['0', '4', '8', '10', '12', '14', '16', '18', '20', '24', '28' ]
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'Nm',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('Asked_Torque'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataMotores["AskedThrottle"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 1000);
}

function Warnings() {

	
	if (mysqlDataMotores["Heartbeat"] == 1) {
	    document.getElementById("Teensy_Motores").className = "green_led led"
	} else {
	    document.getElementById("Teensy_Motores").className = "red_led led"
	}
}