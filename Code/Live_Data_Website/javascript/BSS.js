/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/


google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.load('current', { 'packages': ['gauge'] });
google.charts.setOnLoadCallback(SOC_BMS);
google.charts.setOnLoadCallback(VOLTAGE_BMS);
google.charts.setOnLoadCallback(IBAT_BMS);
google.charts.setOnLoadCallback(Power_BMS);
google.charts.setOnLoadCallback(SOLAR_POWER);
google.charts.setOnLoadCallback(Cell_Votages);
google.charts.setOnLoadCallback(Thermistors);
google.charts.setOnLoadCallback(V_Painel1);
google.charts.setOnLoadCallback(V_Painel2);
google.charts.setOnLoadCallback(V_Painel3);
google.charts.setOnLoadCallback(V_Painel4);
google.charts.setOnLoadCallback(V_Painel5);


var mysqlDataBMS;
var maxVoltage;
var minVoltage;

setInterval(function() {
    $.ajax({
        url: "php/BSS.php",
        dataType: "JSON",
        data: {},
        success: function(x) {
            mysqlDataBMS = x;
        }
    });
}, 300);


function SOC_BMS() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['SOC', -500]
    ]);

    var options = {
        width: 1000,
        height: 200,
        redFrom: 0,
        redTo: 10,
        yellowFrom: 10,
        yellowTo: 25,
        minorTicks: 5,
        majorTicks: ['0', '25', '50', '75', '100']
    };

    var formatter = new google.visualization.NumberFormat({
        suffix: '%',
        fractionDigits: 0
    });

    formatter.format(data, 1);

    var chart = new google.visualization.Gauge(document.getElementById('SOC_BMS'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["SOC"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);

}

function VOLTAGE_BMS() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Voltage', -500]
    ]);
    var options = {
        width: 1000,
        height: 200,
        min: 35,
        max: 51,
        redFrom: 50.4, redTo: 51,
        yellowFrom: 35, yellowTo: 36, yellowColor: '#DC3912',
        
        minorTicks: 4,
        majorTicks: ['35', '39', '43', '47', '51']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 2,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('VOLTAGE_BMS'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["Voltage"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}


function IBAT_BMS() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['I BAT', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: -350, max: 50,
        redFrom: -350, redTo: -300,
        yellowFrom: -300, yellowTo: -200,
        greenFrom: 0, greenTo: 50,
        minorTicks: 10,
        majorTicks: ['-350', '-250', '-150', '-50', '50']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'A',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('IBAT_BMS'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, mysqlDataBMS["Current"]);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function Power_BMS() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Power', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: -10, max: 2,
        //redFrom: -350, redTo: -300,
        //yellowFrom: -300, yellowTo: -200,
        greenFrom: 0, greenTo: 2,
        minorTicks: 2,
        majorTicks: ['-10', '-8', '-6', '-4', '-2', '0','2']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'kW',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('Power_BMS'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( (mysqlDataBMS["Current"] * mysqlDataBMS["Voltage"]))/1000);
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function SOLAR_POWER() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Solar', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 2,
        //redFrom: -350, redTo: -300,
        //yellowFrom: -300, yellowTo: -200,
        greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '0,5', '1', '1,5', '2']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'kW',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('SOLAR_POWER'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["SOLAR"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function Warnings() {
	var motores = (mysqlDataBMS["Warnings"] & 32) >> 5;
	var paineis = (mysqlDataBMS["Warnings"] & 16) >> 4;
	var UV = (mysqlDataBMS["Warnings"] & 8) >> 3;
	var OV = (mysqlDataBMS["Warnings"] & 4) >> 2;
	var OT = (mysqlDataBMS["Warnings"] & 2) >> 1;
	var OC = (mysqlDataBMS["Warnings"] & 1);
	
	if (motores) {
	    document.getElementById("Motores").className = "orange_led led"
	} else {
	    document.getElementById("Motores").className = "green_led led"
	}
	if (paineis) {
	    document.getElementById("Paineis").className = "orange_led led"
	} else {
	    document.getElementById("Paineis").className = "green_led  led"
	}
	if (UV) {
	    document.getElementById("UV").className = "orange_led led"
	} else {
	    document.getElementById("UV").className = "green_led  led"
	}
	if (OV) {
	    document.getElementById("OV").className = "orange_led led"
	} else {
	    document.getElementById("OV").className = "green_led  led"
	}
	if (OT) {
	    document.getElementById("OT").className = "orange_led led"
	} else {
	    document.getElementById("OT").className = "green_led  led"
	}
	if (OC) {
	    document.getElementById("OC").className = "orange_led led"
	} else {
	    document.getElementById("OC").className = "green_led led"
	}
	
}


function Thermistors() {
    var data = google.visualization.arrayToDataTable([
        [{ label: 'Thermistors', type: 'number' }, { label: 'Temperature', type: 'number' }]
    ]);
    data.addRows([
		[1, parseInt(0)],[2, parseInt(0)],[3, parseInt(0)],[4, parseInt(0)],[5, parseInt(0)],[6, parseInt(0)],[7, parseInt(0)],
		[8, parseInt(0)],[9, parseInt(0)],[10, parseInt(0)],[11, parseInt(0)],[12, parseInt(0)],[13, parseInt(0)],[14, parseInt(0)],
        [15, parseInt(0)],[16, parseInt(0)],[17, parseInt(0)],[18, parseInt(0)],[19, parseInt(0)],[20, parseInt(0)],[21, parseInt(0)],
        [22, parseInt(0)],[23, parseInt(0)],[24, parseInt(0)],[25, parseInt(0)],[26, parseInt(0)],[27, parseInt(0)],[28, parseInt(0)],
        [29, parseInt(0)],[30, parseInt(0)],[31, parseInt(0)],[32, parseInt(0)]
    ]);
	var formatter = new google.visualization.NumberFormat({
        suffix: ' ºC',
        fractionDigits: 1
    });
    formatter.format(data, 1);
	var options = {
		title: 'Baterry Temperatures',
		legend: { position: 'none' },
		hAxis: { title: 'Thermistors', viewWindow: { min: 0, max: 33 }, gridlines: { count: 0 }, ticks: [ 1,5,10,15,20,25,30 ] },
		vAxis: { title: 'Temperatures [ºC]', viewWindow: { min: 0, max: 60 }, gridlines: { count: 7 }, ticks: [0, 10, 20, 30, 40, 50, 60] },
		chartArea: { left: '8%', top: '8%',bottom:'8%', width: "90%", height: "70%", backgroundColor: {stroke: '#000', strokeWidth: 2} },
		height: 500,
		width: 600
	};

      var chart = new google.visualization.ColumnChart(document.getElementById('Thermistors'));

      chart.draw(data, options);
      setInterval(function() {
        data.setValue(0, 1,  parseFloat(mysqlDataBMS["T1"]));
        data.setValue(1, 1,  parseFloat(mysqlDataBMS["T2"]));
        data.setValue(2, 1,  parseFloat(mysqlDataBMS["T3"]));
        data.setValue(3, 1,  parseFloat(mysqlDataBMS["T4"]));
        data.setValue(4, 1,  parseFloat(mysqlDataBMS["T5"]));
        data.setValue(5, 1,  parseFloat(mysqlDataBMS["T6"]));
        data.setValue(6, 1,  parseFloat(mysqlDataBMS["T7"]));
        data.setValue(7, 1,  parseFloat(mysqlDataBMS["T8"]));
        data.setValue(8, 1,  parseFloat(mysqlDataBMS["T9"]));
        data.setValue(9, 1,  parseFloat(mysqlDataBMS["T10"]));
        data.setValue(10, 1,  parseFloat(mysqlDataBMS["T11"]));
        data.setValue(11, 1,  parseFloat(mysqlDataBMS["T12"]));
        data.setValue(12, 1,  parseFloat(mysqlDataBMS["T13"]));
        data.setValue(13, 1,  parseFloat(mysqlDataBMS["T14"]));
        data.setValue(14, 1,  parseFloat(mysqlDataBMS["T15"]));
        data.setValue(15, 1,  parseFloat(mysqlDataBMS["T16"]));
        data.setValue(16, 1,  parseFloat(mysqlDataBMS["T17"]));
        data.setValue(17, 1,  parseFloat(mysqlDataBMS["T18"]));
        data.setValue(18, 1,  parseFloat(mysqlDataBMS["T19"]));
        data.setValue(19, 1,  parseFloat(mysqlDataBMS["T20"]));
        data.setValue(20, 1,  parseFloat(mysqlDataBMS["T21"]));
        data.setValue(21, 1,  parseFloat(mysqlDataBMS["T22"]));
        data.setValue(22, 1,  parseFloat(mysqlDataBMS["T23"]));
        data.setValue(23, 1,  parseFloat(mysqlDataBMS["T24"]));
        data.setValue(24, 1,  parseFloat(mysqlDataBMS["T25"]));
        data.setValue(25, 1,  parseFloat(mysqlDataBMS["T26"]));
        data.setValue(26, 1,  parseFloat(mysqlDataBMS["T27"]));
        data.setValue(27, 1,  parseFloat(mysqlDataBMS["T28"]));
        data.setValue(28, 1,  parseFloat(mysqlDataBMS["T29"])); 
        data.setValue(29, 1,  parseFloat(mysqlDataBMS["T30"]));               
        data.setValue(30, 1,  parseFloat(mysqlDataBMS["T31"]));
        data.setValue(31, 1,  parseFloat(mysqlDataBMS["T32"]));
         


        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);

}


function Cell_Votages() {
    var data = google.visualization.arrayToDataTable([
        [{ label: 'Packs', type: 'number' }, { label: 'Voltage', type: 'number' }]
    ]);
    data.addRows([
		[1, parseInt(0)],
        [2, parseInt(0)],
        [3, parseInt(0)],
        [4, parseInt(0)],
        [5, parseInt(0)],
        [6, parseInt(0)],
        [7, parseInt(0)],
        [8, parseInt(0)],
        [9, parseInt(0)],
        [10, parseInt(0)],
        [11, parseInt(0)],
        [12, parseInt(0)]
    ]);
	var formatter = new google.visualization.NumberFormat({
        suffix: ' V',
        fractionDigits: 3
    });
    formatter.format(data, 1);
	var options = {
		title: 'Baterry Packs Voltages',
		legend: { position: 'none' },
		hAxis: { title: 'Pack', viewWindow: { min: 0, max: 13 }, gridlines: { count: 0 }, ticks: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ] },
		vAxis: { title: 'Pack Voltage [V]', viewWindow: { min: 0, max: 5 }, gridlines: { count: 10 }, ticks: [0, 1, 2, 3, 4, 5] },
		chartArea: { left: '8%', top: '8%',bottom:'8%', width: "90%", height: "70%", backgroundColor: {stroke: '#000', strokeWidth: 2} },
		height: 500,
		width: 600
	};

      var chart = new google.visualization.ColumnChart(document.getElementById('Cell_Voltages'));

      chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1,  parseFloat(mysqlDataBMS["V1"]/1000));
        data.setValue(1, 1,  parseFloat(mysqlDataBMS["V2"]/1000));
        data.setValue(2, 1,  parseFloat(mysqlDataBMS["V3"]/1000));
        data.setValue(3, 1,  parseFloat(mysqlDataBMS["V4"]/1000));
        data.setValue(4, 1,  parseFloat(mysqlDataBMS["V5"]/1000));
        data.setValue(5, 1,  parseFloat(mysqlDataBMS["V6"]/1000));
        data.setValue(6, 1,  parseFloat(mysqlDataBMS["V7"]/1000));
        data.setValue(7, 1,  parseFloat(mysqlDataBMS["V8"]/1000));
        data.setValue(8, 1,  parseFloat(mysqlDataBMS["V9"]/1000));
        data.setValue(9, 1,  parseFloat(mysqlDataBMS["V10"]/1000));
        data.setValue(10, 1,  parseFloat(mysqlDataBMS["V11"]/1000));
        data.setValue(11, 1,  parseFloat(mysqlDataBMS["V12"]/1000));
        formatter.format(data, 1);
        chart.draw(data, options);
        maxVoltage = data.getColumnRange(1).max;
        minVoltage = data.getColumnRange(1).min;

    }, 300);

}

function V_Painel1() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['V SP1', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 60,
        redFrom: 52, redTo: 60,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('V_Painel1'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["V_Painel1"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function V_Painel2() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['V SP2', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 60,
        redFrom: 52, redTo: 60,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('V_Painel2'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["V_Painel2"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function V_Painel3() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['V SP3', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 60,
        redFrom: 52, redTo: 60,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('V_Painel3'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["V_Painel3"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function V_Painel4() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['V SP4', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 60,
        redFrom: 52, redTo: 60,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('V_Painel4'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["V_Painel4"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}

function V_Painel5() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['V SP5', -500]
    ]);
    var options = {
        width: 1000, height: 200,
        min: 0, max: 60,
        redFrom: 52, redTo: 60,
        //yellowFrom: -300, yellowTo: -200,
        //greenFrom: 0, greenTo: 2,
        minorTicks: 5,
        majorTicks: ['0', '10', '20', '30', '40', '50', '60']
    };
    var formatter = new google.visualization.NumberFormat({
        suffix: 'V',
        fractionDigits: 1,
        decimalSymbol: ','
    });
    formatter.format(data, 1);
    var chart = new google.visualization.Gauge(document.getElementById('V_Painel5'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, parseFloat( mysqlDataBMS["V_Painel5"]/1000 ));
        formatter.format(data, 1);
        chart.draw(data, options);
    }, 300);
}