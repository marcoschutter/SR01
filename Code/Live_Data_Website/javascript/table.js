/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat 
*/

google.charts.load('current', { 'packages': ['table'] });
google.charts.setOnLoadCallback(drawTable);

function drawTable() {

    var mysqlData;
    $.ajax({
        url: "php/table.php",
        dataType: "JSON",
        async: false,
        data: {},
        success: function(x) {
            mysqlData = x;
        }
    });

    if (mysqlData == null) {
        alert("Não há dados a apresentar!");
        return;
    }

    console.log(mysqlData);

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'SOC [%]');
    data.addColumn('string', 'Voltage [V]');
    data.addColumn('string', 'RPM');
    data.addColumn('string', 'Speed [kN]');
    data.addColumn('string', 'T Mot [ºC]');
    data.addColumn('string', 'T Bat [ºC]');
    data.addColumn('string', 'T BMS [ºC]');
    data.addColumn('string', 'T TD [ºC]');
    data.addColumn('string', 'Motor [A]');
    data.addColumn('string', 'Paineis [A]');
    data.addColumn('string', 'BAT [A]');
    data.addColumn('string', 'MSG');
    data.addColumn('string', 'LAT');
    data.addColumn('string', 'LON');
    data.addColumn('string', 'time');

    var phpDate;
    for (i = 0; i < Object.keys(mysqlData).length; i++) {
        phpDate = mysqlData[i];
        data.addRows([
            [phpDate["SOC"], phpDate["Voltage"], phpDate["RPM"], phpDate["Speed"],
                phpDate["T_Mot"], phpDate["T_Bat"], phpDate["T_BMS"], phpDate["T_TD"],
                phpDate["Motor"], phpDate["Paineis"], phpDate["BAT"], phpDate["MSG"], phpDate["LAT"], 
                phpDate["LON"], phpDate["time"]
            ],
        ]);
    }
    var table = new google.visualization.Table(document.getElementById('table_div'));

    table.draw(data, { showRowNumber: true, width: '100%', height: '100%' });
}