/*
    Copyright (C) 2018  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat  
*/


/*
 * TSB
 * Code for the communication between the BMS 
 * and the teensy.
 * 
 */

#include <TSB_BSS.h>
#include <TSB_datalogger.h>

#define Waiting_time 180000 //time waiting for RPI time message
int time_flag = 0;

IntervalTimer BSS_Timer;
TSB_BSS bss;
TSB_SERIAL tsb_serial_class;
tsb_serial_msg_t tsb_msg;




void setup() {
  Serial.begin(115200);      
  Serial1.begin(115200);
  pinMode(led, OUTPUT);
  pinMode(ignition_pin, OUTPUT);
  pinMode(gate_mos, OUTPUT);
  pinMode(MS0, OUTPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(MS3, OUTPUT); 
  digitalWrite(ignition_pin, HIGH); //Allow BMS to close Motor relay
  digitalWrite(gate_mos, LOW); //Enables charging from solarpanels
  digitalWrite(led, HIGH);
  
  
  
  int initial_time = millis();
  while(Serial.available() <= 0 && time_flag==0){
      if(millis()-initial_time > Waiting_time) time_flag=1;
  }
  if(time_flag==0){
      tsb_msg=tsb_serial_class.receive_msg();
      setTime(tsb_msg.data[0],tsb_msg.data[1],tsb_msg.data[2],tsb_msg.data[3],tsb_msg.data[4],2000+tsb_msg.data[5]);
  }
  
  initSD();
  initLOG();
  analogReadResolution(12); //set the resolution of the ADC to 12bit
  analogReadAveraging(16);  //set the internal moving Average to 16 samples
  BSS_Timer.begin(send_Heartbeat, hb_rate*1000000);

}

void loop() {
 
  digitalWrite(led,  !digitalRead(led));
  bss.get_status();
  bss.get_stateofcharge();
  bss.get_voltage();
  bss.get_current();
  bss.get_temperatures();
  bss.get_cellsvoltages();
  bss.get_solarpower();
  bss.get_solarvoltages();
  bss.check_warnings();
  Data = bss.createData();
  writeLogEntry();

  
  if(Serial) bss.send_Message();

  //bss.SerialPrint();   //DEBUGGING ONLY
  /*
  digitalWriteFast(MS0,LOW);
  digitalWriteFast(MS1,LOW);
  digitalWriteFast(MS2,LOW);
  digitalWriteFast(MS3,LOW);
 
  float volt_out= analogRead(A10)*(3.3/4095.0);
  float R_therm= (10000.0*(volt_out/3.3))/(1-(volt_out/3.3));
  float temp = Beta1/(log(R_therm/(R01*exp(-Beta1/T0))))-273.15;
  //Serial.println(volt_out,4);
 // Serial.println(R_therm);
  Serial.println(temp);
*/
  
}

